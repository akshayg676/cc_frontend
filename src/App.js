import Sidebar from "./components/sidebar/Sidebar";
import Topbar from "./components/topbar/Topbar";
import "./App.css";
import Home from "./pages/home/Home";
import Login from "./pages/Auth/Login/Login";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import UserList from "./pages/Users/userList/UserList";
import User from "./pages/Users/user/User";
import NewUser from "./pages/Users/newUser/NewUser";
import DoctorList from "./pages/Doctors/doctorsList/doctorsList.jsx";
import Doctor from "./pages/Doctors/doctor/doctor.jsx";
import NewDoctor from "./pages/Doctors/newDoctor/NewDoctor.jsx";

function App() {
  return (
    <Router>
      <Topbar />
      <div className="container">
        <Switch>
          <Route exact path="/">
            <Sidebar />
            <Home />
          </Route>
          <Route exact path="/login">
            <Login />
          </Route>
          <Route path="/users">
            <Sidebar />
            <UserList />
          </Route>
          <Route path="/user/:userId">
            <Sidebar />
            <User />
          </Route>
          <Route path="/newUser">
            <Sidebar />
            <NewUser />
          </Route>
          <Route path="/doctors">
            <Sidebar />
            <DoctorList />
          </Route>
          <Route path="/product/:productId">
            <Sidebar />
            <Doctor />
          </Route>
          <Route path="/newproduct">
            <Sidebar />
            <NewDoctor />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
