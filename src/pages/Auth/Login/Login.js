import React from "react";
import "./login.css";
import { useEffect, useState } from "react";
import axios from "axios";

const Login = ({ history }) => {
  const [user, setUser] = useState({
    username: "",
    password: "",
  });

  useEffect(() => {
    if (localStorage.getItem("authToken")) {
      window.location.href = "/";
    }
  }, [history]);

  const loginSubmit = async (e) => {
    e.preventDefault();
    try {
      const params = new URLSearchParams();
      console.log(user.password);
      params.append("grant_type", "password");
      params.append("username", user.username);
      params.append("password", user.password);
      axios({
        method: "post",
        url: "http://localhost:4000/oauth/token",
        headers: {
          Authorization: "Basic YXBwbGljYXRpb246c2VjcmV0",
          "Content-Type": "application/x-www-form-urlencoded",
        },
        data: params,
      }).then(function (response) {
        //handle success
        localStorage.setItem("authToken", response.data.accessToken);
        window.location.href = "/";
      });
    } catch (err) {
      alert(err.response.data.msg);
    }
  };

  return (
    <div class="cont">
      <div class="form">
        <form action="" onSubmit={loginSubmit}>
          <h1>Login</h1>
          <input
            type="text"
            className="user"
            required
            value={user.username}
            placeholder="Username"
            onChange={(e) => {
              setUser({ ...user, username: e.target.value });
            }}
          />
          <input
            type="password"
            className="pass"
            required
            value={user.password}
            placeholder="Password"
            onChange={(e) => {
              setUser({ ...user, password: e.target.value });
            }}
          />
          <button class="login">Login</button>
        </form>
      </div>
    </div>
  );
};

export default Login;
